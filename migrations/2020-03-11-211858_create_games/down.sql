-- This file should undo anything in `up.sql`
drop table games;
drop type game_mode;
drop type round;
drop type round_state;
drop type player;
