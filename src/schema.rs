table! {
    use diesel::sql_types::*;
    use crate::models::*;

    games (id) {
        id -> Text,
        mode -> GameModeST,
        player_1_wins -> Int4,
        player_2_wins -> Int4,
        round -> RoundST,
    }
}
