use super::{NewGameForm, NewRoundForm, TakeTurnForm};
use crate::error::{APIError, InvalidGameIdError, QueryError};
use crate::models::{Game, Round, TakeTurnSuccess};
use crate::schema;
use crate::DbConn;
use diesel::query_dsl::filter_dsl::FindDsl;
use diesel::result::QueryResult;
use diesel::{self, Insertable, RunQueryDsl, SaveChangesDsl};
use rocket::request::Form;
use rocket::{get, post};
use rocket_contrib::json::Json;

#[post("/new-round", data = "<form>")]
pub fn new_round(db_conn: DbConn, form: Form<NewRoundForm>) -> Result<Json<Round>, APIError> {
	use schema::games::dsl::*;

	let query_result: QueryResult<Game> = games.find(&form.game_id).first(&*db_conn);

	match query_result {
		Ok(mut game) => {
			// Modify the model instance.
			game.new_round();

			// save_changes actually applies the changes to the database.
			match game.save_changes::<Game>(&*db_conn) {
				Ok(game) => Ok(Json(game.round)),
				Err(_) => Err(APIError::QueryError(QueryError::new())),
			}
		}
		Err(diesel::result::Error::NotFound) => Err(APIError::InvalidGameId(
			InvalidGameIdError::new(form.game_id.clone()),
		)),
		_ => Err(APIError::QueryError(QueryError::new())),
	}
}

#[post("/take-turn", data = "<form>")]
pub fn take_turn(
	db_conn: DbConn,
	form: Form<TakeTurnForm>,
) -> Result<Json<TakeTurnSuccess>, APIError> {
	use schema::games::dsl::*;

	let query_result: QueryResult<Game> = games.find(&form.game_id).first(&*db_conn);

	match query_result {
		Ok(mut game) => {
			// Depending on the round state, we need to either perform a turn and return the results or return an error.
			let take_turn_result = game.take_turn(form.player, form.spot);

			match take_turn_result {
				Ok(place_symbol_success) => {
					// save_changes actually applies the changes to the database.
					match game.save_changes::<Game>(&*db_conn) {
						Ok(_) => Ok(Json(place_symbol_success)),
						Err(_) => Err(APIError::QueryError(QueryError::new())),
					}
				}
				Err(error) => Err(error),
			}
		}
		Err(diesel::result::Error::NotFound) => Err(APIError::InvalidGameId(
			InvalidGameIdError::new(form.game_id.clone()),
		)),
		_ => Err(APIError::QueryError(QueryError::new())),
	}
}

#[get("/get?<game_id>")]
pub fn get_game_state(db_conn: DbConn, game_id: String) -> Result<Json<Game>, APIError> {
	use schema::games::dsl::*;

	let query_result: QueryResult<Game> = games.find(&game_id).first(&*db_conn);

	match query_result {
		Ok(game) => Ok(Json(game)),
		Err(diesel::result::Error::NotFound) => {
			Err(APIError::InvalidGameId(InvalidGameIdError::new(game_id)))
		}
		_ => Err(APIError::QueryError(QueryError::new())),
	}
}

#[post("/new", data = "<form>")]
pub fn new_game(db_conn: DbConn, form: Form<NewGameForm>) -> Result<Json<Game>, QueryError> {
	use schema::games::dsl::*;

	// Create the model instance.
	let new_game = Game::new(form.mode);

	// Since it is an Insertable, we can insert it into the database.
	let query_result: QueryResult<Game> = new_game.insert_into(games).get_result(&*db_conn);

	match query_result {
		Ok(game) => Ok(Json(game)),
		_ => Err(QueryError::new()),
	}
}
