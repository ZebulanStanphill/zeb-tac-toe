pub mod api;
pub mod api_db;
pub mod catchers;

use crate::models::GameMode;
use crate::models::Player;
use rocket::FromForm;
use std::fmt;

#[derive(fmt::Debug, FromForm)]
pub struct TakeTurnForm {
	game_id: String,
	player: Player,
	spot: usize,
}

#[derive(fmt::Debug, FromForm)]
pub struct NewGameForm {
	mode: GameMode,
}

#[derive(fmt::Debug, FromForm)]
pub struct NewRoundForm {
	game_id: String,
}
