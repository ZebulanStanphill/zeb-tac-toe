use rocket::http::{ContentType, Status};
use rocket::request::Request;
use rocket::response::{self, Responder, Response};
use serde::Serialize;
use serde_json::json;
use std::error;
use std::fmt;
use std::io::Cursor;

#[derive(fmt::Debug, Serialize)]
pub struct InvalidGameIdError {
	pub id: String,
	pub message: String,
}

impl InvalidGameIdError {
	pub fn new(id: String) -> Self {
		Self {
			id: id.clone(),
			message: format!("Invalid game id: {}", id),
		}
	}
}

impl fmt::Display for InvalidGameIdError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		f.write_str(&self.message)
	}
}

impl error::Error for InvalidGameIdError {}

#[derive(fmt::Debug, Serialize)]
pub struct InvalidMoveError {
	pub spot: usize,
	pub message: String,
}

impl InvalidMoveError {
	pub fn new(spot: usize) -> Self {
		Self {
			spot,
			message: format!("Invalid move; spot {} is already filled.", spot),
		}
	}
}

impl fmt::Display for InvalidMoveError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		f.write_str(&self.message)
	}
}

impl error::Error for InvalidMoveError {}

#[derive(fmt::Debug, Serialize)]
pub struct WrongTurnError {
	pub message: String,
}

impl WrongTurnError {
	pub fn new() -> Self {
		Self::default()
	}
}

impl Default for WrongTurnError {
	fn default() -> Self {
		Self {
			message: "Cannot move out of turn.".to_owned(),
		}
	}
}

impl fmt::Display for WrongTurnError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		f.write_str(&self.message)
	}
}

impl error::Error for WrongTurnError {}

#[derive(fmt::Debug, Serialize)]
pub struct GameAlreadyOverError {
	pub message: String,
}

impl GameAlreadyOverError {
	pub fn new() -> Self {
		Self::default()
	}
}

impl Default for GameAlreadyOverError {
	fn default() -> Self {
		Self {
			message: "Cannot move in finished game.".to_owned(),
		}
	}
}

impl fmt::Display for GameAlreadyOverError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		f.write_str(&self.message)
	}
}

impl error::Error for GameAlreadyOverError {}

#[derive(fmt::Debug, Serialize)]
pub struct QueryError {
	pub message: String,
}

impl QueryError {
	pub fn new() -> Self {
		Self::default()
	}
}

impl Default for QueryError {
	fn default() -> Self {
		Self {
			message: "A server-side error occurred while trying to interact with the database."
				.to_owned(),
		}
	}
}

impl fmt::Display for QueryError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		f.write_str(&self.message)
	}
}

impl error::Error for QueryError {}

#[derive(fmt::Debug, Serialize)]
#[serde(tag = "type")]
pub enum APIError {
	GameAlreadyOver(GameAlreadyOverError),
	InvalidGameId(InvalidGameIdError),
	InvalidMove(InvalidMoveError),
	QueryError(QueryError),
	WrongTurn(WrongTurnError),
}

impl<'r> Responder<'r> for APIError {
	fn respond_to(self, _: &Request) -> response::Result<'r> {
		match self {
			Self::GameAlreadyOver(_)
			| Self::InvalidGameId(_)
			| Self::InvalidMove(_)
			| Self::WrongTurn(_) => Response::build()
				.header(ContentType::JSON)
				.sized_body(Cursor::new(json!({ "error": self }).to_string()))
				.status(Status::BadRequest)
				.ok(),
			Self::QueryError(_) => Response::build()
				.header(ContentType::JSON)
				.sized_body(Cursor::new(json!({ "error": self }).to_string()))
				.status(Status::InternalServerError)
				.ok(),
		}
	}
}
