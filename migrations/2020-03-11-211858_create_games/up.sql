create type game_mode as enum ('player_vs_player', 'player_vs_computer');
create type round_state as enum (
	'player_1_turn',
	'player_2_turn',
	'player_1_win',
	'player_2_win',
	'tie'
);
create type player as enum ('player_1', 'player_2');
create type round as (round_state round_state, round_spots player []);
create table games (
	id text primary key,
	mode game_mode not null,
	player_1_wins int not null,
	player_2_wins int not null,
	round round not null
);
