use super::{NewGameForm, NewRoundForm, TakeTurnForm};
use crate::error::{APIError, InvalidGameIdError};
use crate::models::{Game, Round, TakeTurnSuccess};
use rocket::request::Form;
use rocket::{get, post, State};
use rocket_contrib::json::Json;
use std::collections::HashMap;
use std::sync::Mutex;

type SharedGameInstances = Mutex<HashMap<String, Game>>;

#[post("/new-round", data = "<form>")]
pub fn new_round(
	form: Form<NewRoundForm>,
	instances: State<SharedGameInstances>,
) -> Result<Json<Round>, InvalidGameIdError> {
	let mut instances = instances.lock().expect("to lock managed state");

	match instances.get_mut(&form.game_id) {
		Some(game) => {
			game.new_round();

			Ok(Json(game.round.clone()))
		}
		None => Err(InvalidGameIdError::new(form.game_id.clone())),
	}
}

#[post("/take-turn", data = "<form>")]
pub fn take_turn(
	form: Form<TakeTurnForm>,
	instances: State<SharedGameInstances>,
) -> Result<Json<TakeTurnSuccess>, APIError> {
	let mut instances = instances.lock().expect("to lock managed state");

	match instances.get_mut(&form.game_id) {
		Some(game) => {
			// Depending on the round state, we need to either perform a turn and return the results or return an error.
			match game.take_turn(form.player, form.spot) {
				Ok(take_turn_success) => Ok(Json(take_turn_success)),
				Err(error) => Err(error),
			}
		}
		None => Err(APIError::InvalidGameId(InvalidGameIdError::new(
			form.game_id.clone(),
		))),
	}
}

#[get("/get?<game_id>")]
pub fn get_game_state(
	game_id: String,
	instances: State<SharedGameInstances>,
) -> Result<Json<Game>, InvalidGameIdError> {
	let instances = instances.lock().expect("to lock managed state");

	match instances.get(&game_id) {
		Some(game) => Ok(Json(game.clone())),
		None => Err(InvalidGameIdError::new(game_id)),
	}
}

#[post("/new", data = "<form>")]
pub fn new_game(form: Form<NewGameForm>, instances: State<SharedGameInstances>) -> Json<Game> {
	let mut instances = instances.lock().expect("lock managed state");
	let game = Game::new(form.mode);
	instances.insert(game.id.clone(), game.clone());

	Json(game)
}
