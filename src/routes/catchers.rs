use rocket::catch;

#[catch(400)]
pub fn invalid_input() -> &'static str {
	"Error 400: bad request."
}

#[catch(404)]
pub fn not_found() -> &'static str {
	"Error 404: page not found! https://invidio.us/watch?v=1b_cKPfn9YQ"
}
