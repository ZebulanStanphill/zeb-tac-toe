use crate::error::{APIError, GameAlreadyOverError, InvalidMoveError, WrongTurnError};
use crate::schema::games;
use diesel::deserialize::{self, FromSql};
use diesel::pg::Pg;
use diesel::serialize::{self, IsNull, Output, ToSql, WriteTuple};
use diesel::sql_types::{Array, Nullable};
use diesel::{not_none, SqlType};
use nanoid::nanoid;
use rocket::http::RawStr;
use rocket::request::FromParam;
use rocket::FromFormValue;
use serde::Serialize;
use std::fmt;
use std::io::Write;

// I don't fully understand the ToSql and FromSql implementations, but that's okay, because I don't have to! I mostly just adapted the code from https://github.com/diesel-rs/diesel/blob/1.4.x/diesel_tests/tests/custom_types.rs
// Note that this code will have to be update when diesel 2.0 is released due to breaking changes; see the master branch version of the aforementioned file.
// I initially tried to use diesel-derive-enum instead of doing this manually, but that crate doesn't seem to work properly with the Insertable trait.

// ST is short for "Sql type". We can't map our enums/structs to database types directly, so we have to use proxy structs like PlayerST.

#[derive(SqlType)]
#[postgres(type_name = "player")]
pub struct PlayerST;

#[derive(AsExpression, Clone, Copy, fmt::Debug, FromFormValue, FromSqlRow, PartialEq, Serialize)]
#[sql_type = "PlayerST"]
pub enum Player {
	Player1,
	Player2,
}

use Player::{Player1, Player2};

impl Player {
	// Get the other player.
	fn other(self) -> Self {
		match self {
			Self::Player1 => Player2,
			Self::Player2 => Player1,
		}
	}
}

impl fmt::Display for Player {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match self {
			Self::Player1 => f.write_str("player 1"),
			Self::Player2 => f.write_str("player 2"),
		}
	}
}

impl ToSql<PlayerST, Pg> for Player {
	fn to_sql<W: Write>(&self, out: &mut Output<W, Pg>) -> serialize::Result {
		match *self {
			Player1 => out.write_all(b"player_1")?,
			Player2 => out.write_all(b"player_2")?,
		}
		Ok(IsNull::No)
	}
}

impl FromSql<PlayerST, Pg> for Player {
	fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
		match not_none!(bytes) {
			b"player_1" => Ok(Player1),
			b"player_2" => Ok(Player2),
			_ => Err("Unrecognized enum variant".into()),
		}
	}
}

#[derive(SqlType)]
#[postgres(type_name = "round_state")]
pub struct RoundStateST;

#[derive(AsExpression, Clone, Copy, fmt::Debug, FromSqlRow, PartialEq, Serialize)]
#[sql_type = "RoundStateST"]
pub enum RoundState {
	#[serde(rename = "turn")]
	Turn(Player),
	#[serde(rename = "win")]
	Win(Player),
	#[serde(rename = "tie")]
	Tie,
}

impl ToSql<RoundStateST, Pg> for RoundState {
	fn to_sql<W: Write>(&self, out: &mut Output<W, Pg>) -> serialize::Result {
		match *self {
			Self::Turn(player) => match player {
				Player1 => out.write_all(b"player_1_turn")?,
				Player2 => out.write_all(b"player_2_turn")?,
			},
			Self::Win(player) => match player {
				Player1 => out.write_all(b"player_1_win")?,
				Player2 => out.write_all(b"player_2_win")?,
			},
			Self::Tie => out.write_all(b"tie")?,
		}
		Ok(IsNull::No)
	}
}

impl FromSql<RoundStateST, Pg> for RoundState {
	fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
		match not_none!(bytes) {
			b"player_1_turn" => Ok(Self::Turn(Player1)),
			b"player_2_turn" => Ok(Self::Turn(Player2)),
			b"player_1_win" => Ok(Self::Win(Player1)),
			b"player_2_win" => Ok(Self::Win(Player2)),
			b"tie" => Ok(Self::Tie),
			_ => Err("Unrecognized enum variant".into()),
		}
	}
}

type SpotsST = Array<Nullable<PlayerST>>;

type Spots = Vec<Option<Player>>;

#[derive(SqlType, QueryId, Debug, Clone, Copy)]
#[postgres(type_name = "round")]
pub struct RoundST;

#[derive(AsExpression, Clone, fmt::Debug, FromSqlRow, Serialize)]
#[sql_type = "RoundST"]
pub struct Round {
	pub state: RoundState,
	// PostgreSQL arrays are mapped to Rust vectors.
	pub spots: Spots,
}

impl Round {
	pub fn new() -> Self {
		Self::default()
	}
}

impl Default for Round {
	fn default() -> Self {
		Self {
			spots: vec![None; 9],
			state: RoundState::Turn(Player1),
		}
	}
}

// See https://github.com/diesel-rs/diesel/blob/1.4.x/diesel/src/pg/types/record.rs#L229-L251
impl ToSql<RoundST, Pg> for Round {
	fn to_sql<W: Write>(&self, out: &mut Output<W, Pg>) -> serialize::Result {
		WriteTuple::<(RoundStateST, SpotsST)>::write_tuple(&(self.state, self.spots.clone()), out)
	}
}

impl FromSql<RoundST, Pg> for Round {
	fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
		// Based on https://gitter.im/diesel-rs/diesel?at=5d8a68c0be709e39d1872f29
		let (state, spots) = <(RoundState, Spots)>::from_sql(bytes)?;

		Ok(Self { state, spots })
	}
}

#[derive(SqlType)]
#[postgres(type_name = "game_mode")]
pub struct GameModeST;

#[derive(AsExpression, Clone, Copy, fmt::Debug, FromFormValue, FromSqlRow, PartialEq, Serialize)]
#[sql_type = "GameModeST"]
pub enum GameMode {
	#[form(value = "pvp")]
	PlayerVsPlayer,
	#[form(value = "pvc")]
	PlayerVsComputer,
}

// I basically just modified the example at https://api.rocket.rs/v0.4/rocket/request/trait.FromParam.html
impl<'r> FromParam<'r> for GameMode {
	type Error = &'r RawStr;

	fn from_param(param: &'r RawStr) -> Result<Self, Self::Error> {
		if param == "pvp" {
			Ok(Self::PlayerVsPlayer)
		} else if param == "pvc" {
			Ok(Self::PlayerVsComputer)
		} else {
			Err(param)
		}
	}
}

impl ToSql<GameModeST, Pg> for GameMode {
	fn to_sql<W: Write>(&self, out: &mut Output<W, Pg>) -> serialize::Result {
		match *self {
			Self::PlayerVsPlayer => out.write_all(b"player_vs_player")?,
			Self::PlayerVsComputer => out.write_all(b"player_vs_computer")?,
		}
		Ok(IsNull::No)
	}
}

impl FromSql<GameModeST, Pg> for GameMode {
	fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
		match not_none!(bytes) {
			b"player_vs_player" => Ok(Self::PlayerVsPlayer),
			b"player_vs_computer" => Ok(Self::PlayerVsComputer),
			_ => Err("Unrecognized enum variant".into()),
		}
	}
}

type ThreeSlotPattern = [usize; 3];

const WIN_PATTERNS: [ThreeSlotPattern; 8] = [
	// Horizontal
	[0, 1, 2],
	[3, 4, 5],
	[6, 7, 8],
	// Vertical
	[0, 3, 6],
	[1, 4, 7],
	[2, 5, 8],
	// Diagonal
	[0, 4, 8],
	[2, 4, 6],
];

#[derive(Serialize)]
pub enum TakeTurnSuccess {
	#[serde(rename = "win")]
	Win {
		pattern: ThreeSlotPattern,
		winner: Player,
	},
	#[serde(rename = "tie")]
	Tie,
	#[serde(rename = "next_turn")]
	NextTurn { next_player: Player },
}

// Property order is important here since it corresponds with the column order in the database table.
#[derive(AsChangeset, Clone, Identifiable, Insertable, Queryable, Serialize)]
#[table_name = "games"]
pub struct Game {
	pub id: String,
	pub mode: GameMode,
	// It has to be i32 rather than u32 because PostgreSQL doesn't have unsigned integer types.
	pub player_1_wins: i32,
	pub player_2_wins: i32,
	pub round: Round,
}

impl Game {
	pub fn new(mode: GameMode) -> Self {
		Self {
			id: nanoid!(12),
			mode,
			player_1_wins: 0,
			player_2_wins: 0,
			round: Round::new(),
		}
	}

	pub fn new_round(&mut self) {
		self.round = Round::new();
	}

	pub fn take_turn(
		&mut self,
		moving_player: Player,
		spot: usize,
	) -> Result<TakeTurnSuccess, APIError> {
		// Depending on the round state, we need to either perform a turn and return the results or return an error.
		match self.round.state {
			RoundState::Turn(player) => {
				// Check if it is the player's turn.
				if moving_player == player {
					// Check if the move is valid.
					if self.round.spots[spot] == None {
						// Place symbol.
						self.round.spots[spot] = match player {
							Player1 => Some(Player1),
							Player2 => Some(Player2),
						};

						// Check for win.
						let matching_win_pattern = WIN_PATTERNS.iter().find(|pattern| {
							pattern
								.iter()
								.all(|spot_index| self.round.spots[*spot_index] == Some(player))
						});

						match matching_win_pattern {
							// If there is a matching win pattern, then the game has been won.
							Some(pattern) => {
								self.round.state = RoundState::Win(player);
								match player {
									Player1 => self.player_1_wins += 1,
									Player2 => self.player_2_wins += 1,
								};

								Ok(TakeTurnSuccess::Win {
									pattern: *pattern,
									winner: player,
								})
							}
							None => {
								// If no spots are empty, it is a tie.
								if self.round.spots.iter().all(|spot| spot.is_some()) {
									self.round.state = RoundState::Tie;

									Ok(TakeTurnSuccess::Tie)
								} else {
									// Otherwise, the game continues.

									let next_player = player.other();

									self.round.state = RoundState::Turn(next_player);

									Ok(TakeTurnSuccess::NextTurn { next_player })
								}
							}
						}
					} else {
						Err(APIError::InvalidMove(InvalidMoveError::new(spot)))
					}
				} else {
					Err(APIError::WrongTurn(WrongTurnError::new()))
				}
			}
			_ => Err(APIError::GameAlreadyOver(GameAlreadyOverError::new())),
		}
	}
}
