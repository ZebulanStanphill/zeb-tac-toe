// Rocket currently requires both of these unstable Rust features. Rocket 0.5.x will not require decl_macro.
#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate diesel;

mod error;
mod models;
mod routes;
mod schema;

use dotenv::dotenv;
use rocket::{self, catchers, routes};
use rocket_contrib::database;
use rocket_contrib::databases::diesel::PgConnection;
use rocket_contrib::serve::StaticFiles;
use routes::{api, api_db, catchers};
use std::collections::HashMap;
use std::sync::Mutex;

#[database("zeb_tac_toe_db")]
pub struct DbConn(PgConnection);

// Whether to use the PostgreSQL database or temporary managed state.
const USE_DB: bool = true;

fn main() {
	if USE_DB {
		dotenv().ok();

		rocket::ignite()
			.attach(DbConn::fairing())
			.mount("/", StaticFiles::from("static"))
			.mount(
				"/api/game",
				routes![
					api_db::get_game_state,
					api_db::new_game,
					api_db::new_round,
					api_db::take_turn
				],
			)
			.register(catchers![catchers::invalid_input, catchers::not_found])
			.launch();
	} else {
		rocket::ignite()
			// The HashMap has to be wrapped in a Mutex to avoid potential thread safety issues. See https://github.com/SergioBenitez/Rocket/issues/359
			.manage(Mutex::new(HashMap::<String, models::Game>::new()))
			.mount("/", StaticFiles::from("static"))
			.mount(
				"/api/game",
				routes![
					api::get_game_state,
					api::new_game,
					api::new_round,
					api::take_turn
				],
			)
			.register(catchers![catchers::invalid_input, catchers::not_found])
			.launch();
	}
}
